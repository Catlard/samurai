﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrigger : MonoBehaviour {

	private bool _consume = true;

	public void OnTriggerEnter2D(Collider2D c) {
		if (!_consume)
			return;

		_consume = false;

		if (c.gameObject.name == "Samurai") {
			GameObject.Find ("MVC").GetComponent<Controller> ().OnAudioTrigger (gameObject.name);
		}
	}
}
