﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	private float _walkSpeed = 5f;
	private Rigidbody2D _body;

	public void Init() {
		_body = GetComponent<Rigidbody2D> ();
	}

	private bool ReactToKeyboardInput() {

		Vector3 movement = Vector3.zero;
		if (Input.GetKey (KeyCode.LeftArrow)) {
			movement.x -= _walkSpeed;
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			movement.x += _walkSpeed;
		}
		if (Input.GetKey (KeyCode.UpArrow)) {
			movement.y += _walkSpeed;
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			movement.y -= _walkSpeed;
		}

		//deltatime is a per-machine timing, causes the movement to look smooth.
		_body.velocity = movement;

		return true;
	}

	private bool ReactToMouseInput() {
		if (!Input.GetMouseButton (0))
			return false;

		Vector3 currentMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Vector3 dir = currentMousePosition - transform.position;
		dir.z = 0;
		dir = dir.normalized * _walkSpeed;

		_body.velocity = dir;

		return true;
		
	}

	public void Update() {
		if (ReactToMouseInput ()) {
			//do nothing
		} else {
			ReactToKeyboardInput ();
		}
	}


}
