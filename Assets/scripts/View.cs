﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class View : MonoBehaviour {

	private Player _samurai;

	public AudioClip[] _audioClips;
	private AudioSource _source;
	private Model _model;
	private CameraFollowSamurai _cameraFollowSamurai;


	public void Init() {
		_model = GameObject.Find ("MVC").GetComponent<Model> ();
		_source = GameObject.Find ("Main Camera").GetComponent<AudioSource> ();
		_samurai = GameObject.Find ("Samurai").GetComponent<Player> ();
		_samurai.Init ();
		_cameraFollowSamurai = GameObject.Find ("Main Camera").GetComponent<CameraFollowSamurai> ();
		_cameraFollowSamurai.Init ();

		OnAudioTrigger ("StartGame");

	}

	public void OnAudioTrigger(string name) {
		if (_model._mute)
			return;

		foreach (AudioClip c in _audioClips) {
			if (c.name == name) {
				_source.clip = c;
				_source.Play ();
				print ("Playing triggered clip: " + name);
				return;
			}
		}

		Debug.LogWarning ("Couldn't find clip named: " + name);
	}
}
