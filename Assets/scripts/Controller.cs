﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

	private Model _model;
	private View _view;

	// Use this for initialization
	void Start () {
		//find the other instances of model and view.
		_model = GameObject.Find ("MVC").GetComponent<Model> ();
		_view = GameObject.Find ("MVC").GetComponent<View> ();

		//call init functions.
		_model.Init ();
		_view.Init ();
	}

	public void OnAudioTrigger(string name) {
		_view.OnAudioTrigger (name);
	}

}
