﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowSamurai : MonoBehaviour {

	private Vector3 _offsetFromPlayer;
	private Transform _player;

	public void Init() {
		_player = GameObject.Find ("Samurai").transform;
		_offsetFromPlayer = transform.position - _player.position;
	}

	public void Update() {
		Vector3 desired = _player.position + _offsetFromPlayer;
		Vector3 lerped = Vector3.Lerp (transform.position, desired, .25f);
		transform.position = lerped;
	}
}
